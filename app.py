from flask import Flask
import hashlib
import json
import re
from binascii import a2b_base64
from flask import request, make_response
from flask import jsonify
from flask import render_template
from werkzeug.wsgi import SharedDataMiddleware
import os

app = Flask(__name__, static_url_path='/static')
app.debug = True

@app.route('/')
def index():
    return render_template('index2.html')

@app.route('/preview/<imageName>')
def preview(imageName):
    return render_template('preview.html', image=imageName)

@app.route('/trololo')
def trololo():
    try: lst = os.listdir('static/images')
    except OSError:
        pass #ignore errors
    return render_template('images.html', images=lst)

@app.route('/save-img', methods=['POST'])
def save_img():
    if request.method == 'POST':
        data = request.form['image']
        
        matches = re.match(r'data:image\/(gif|jpeg|png);base64,(.*)', data)
        
        binary_data = a2b_base64(matches.group(2))
        filename = hashlib.md5(binary_data).hexdigest() + '.png';
        
        fd = open('static/images/' + filename, 'wb')
        fd.write(binary_data)
        fd.close()
        
        return jsonify({'filename':  filename})


if __name__ == "__main__":
    app.run(host="localhost")
#    app.run(host="10.0.160.54")
