$(window).load(function(){
  // CAN\NVAS.js plugin
  // ninivert, december 2016
  (function (window, document) {
    /**
     * CAN\VAS Plugin - Adding line breaks to canvas
     * @arg {string} [str=Hello World] - text to be drawn
     * @arg {number} [x=0]             - top left x coordinate of the text
     * @arg {number} [y=textSize]      - top left y coordinate of the text
     * @arg {number} [w=canvasWidth]   - maximum width of drawn text
     * @arg {number} [lh=1]            - line height
     * @arg {number} [method=fill]     - text drawing method, if 'none', text will not be rendered
     */

    CanvasRenderingContext2D.prototype.drawBreakingText = function (str, x, y, w, lh, method) {
      // local variables and defaults
      var textSize = parseInt(this.font.replace(/\D/gi, ''));
      var textParts = [];
      var textPartsNo = 0;
      var words = [];
      var currLine = '';
      var testLine = '';
      str = str || '';
      x = x || 0;
      y = y || 0;
      w = w || this.canvas.width;
      lh = lh || 1;
      method = method || 'fill';

      // manual linebreaks
      textParts = str.split('\n');
      textPartsNo = textParts.length;

      // split the words of the parts
      for (var i = 0; i < textParts.length; i++) {
	words[i] = textParts[i].split(' ');
      }

      // now that we have extracted the words
      // we reset the textParts
      textParts = [];

      // calculate recommended line breaks
      // split between the words
      for (var i = 0; i < textPartsNo; i++) {

	// clear the testline for the next manually broken line
	currLine = '';

	for (var j = 0; j < words[i].length; j++) {
	  testLine = currLine + words[i][j] + ' ';

	  // check if the testLine is of good width
	  if (this.measureText(testLine).width > w && j > 0) {
	    textParts.push(currLine);
	    currLine = words[i][j] + ' ';
	  } else {
	    currLine = testLine;
	  }
	}
        // replace is to remove trailing whitespace
	textParts.push(currLine);
      }

      // render the text on the canvas
      for (var i = 0; i < textParts.length; i++) {
	if (method === 'fill') {
	  this.fillText(textParts[i].replace(/((\s*\S+)*)\s*/, '$1'), x, y+(textSize*lh*i));
	} else if (method === 'stroke') {
	  this.strokeText(textParts[i].replace(/((\s*\S+)*)\s*/, '$1'), x, y+(textSize*lh*i));
	} else if (method === 'none') {
          return {'textParts': textParts, 'textHeight': textSize*lh*textParts.length};
	} else {
          console.warn('drawBreakingText: ' + method + 'Text() does not exist');
	  return false;
	}
      }

      return {'textParts': textParts, 'textHeight': textSize*lh*textParts.length};
    };
  }) (window, document);





  var canvas = document.createElement('canvas');
  var canvasWrapper = document.getElementById('canvasWrapper');
  canvasWrapper.appendChild(canvas);
  canvas.width = 300;
  canvas.height = 600;
  canvas.background = 'white'
  var ctx = canvas.getContext('2d');
  var padding = 15;
  var textTop = 'Armand Biglary';
  var textBottom = 'Ma vie mon œuvre';
  var textSizeTop = 6;
  var textSizeBottom = 10;
  var image = document.createElement('img');
  image.crossOrigin = ""; 
  image.src = 'static/goat.jpg';
  var foliob = document.createElement('img');
  foliob.src= 'static/foliob.png'
  var foliow = document.createElement('img');
  foliow.src= 'static/foliow.png'
  var white = document.createElement('img');
  white.src= 'static/white.png'



  image.onload = function (ev) {
    // delete and recreate canvas do untaint it
    canvas.outerHTML = '';
    canvas = document.createElement('canvas');
    canvasWrapper.appendChild(canvas);
    ctx = canvas.getContext('2d');
    document.getElementById('dark').click();
    document.getElementById('dark').click();
    ctx.font = "20px basic-serif";
    draw();
    
  };

  document.getElementById('imgURL').oninput = function(ev) {
    image.src = this.value;
  };

  document.getElementById('imgFile').onchange = function(ev) {
    var reader = new FileReader();
    reader.onload = function(ev) {
      image.src = reader.result;
    };
    reader.readAsDataURL(this.files[0]);
  };



  document.getElementById('textTop').oninput = function(ev) {
    textTop = this.value;
    draw();
  };

  document.getElementById('textBottom').oninput = function(ev) {
    textBottom = this.value;
    draw();
  };



  document.getElementById('textSizeTop').oninput = function(ev) {
    textSizeTop = parseInt(this.value);
    draw();
    document.getElementById('textSizeTopOut').innerHTML = this.value;
  };
  document.getElementById('textSizeBottom').oninput = function(ev) {
    textSizeBottom = parseInt(this.value);
    draw();
    document.getElementById('textSizeBottomOut').innerHTML = this.value;
  };



  document.getElementById('dark').onchange = function(ev) {
    dark = this.checked;
    draw();
  };



  document.getElementById('save').onclick = function () {
    var imageDataUrl = canvas.toDataURL('image/png')

    $.ajax({
      url: "/save-img",
      type: "POST",
      data: {image: imageDataUrl},
      dataType: "json",
      success: function(response){
        window.location = "preview/"+response.filename
      }
    });
  };





  function style(font, size, align, base) {
    ctx.font = size + 'px ' + font;
    ctx.textAlign = align;
    ctx.textBaseline = base;
  }

  function draw() {
    // uppercase the text
    var top = textTop//.toUpperCase();
    var bottom = textBottom//.toUpperCase();
    
    // set appropriate canvas size
    canvas.width = 600;
    canvas.height = 800;

    // draw the image
    if (image.width>=image.height){
      diff = image.width-image.height
      ctx.drawImage(image,
                    diff/2, 0, image.height, image.height ,
                    0, 200, 600, 600);
    }else{

      diff = image.height-image.width
      ctx.drawImage(image,
                    0, diff/2, image.width, image.width ,
                    0, 200, 600, 600);
    }
    //drawImage(image, 0, 200, canvas.width, canvas.height-200);

    //    Draw the Watermark
    if (dark){
      ctx.drawImage(foliob, 0.8 *canvas.width, 0.9* canvas.height, 100, 50);
    } else {
      ctx.drawImage(foliow, 0.8 *canvas.width, 0.9* canvas.height, 100, 50);
    }
    // styles

    var _textSizeTop = textSizeTop/100*canvas.width;
    var _textSizeBottom = textSizeBottom/100*canvas.width;
    ctx.drawImage(white, 0,0, 600, 200 );

    // draw top text
    ctx.fillStyle = 'salmon';
    style('basic-serif', _textSizeTop, 'left', 'bottom');
    ctx.fillText(top, 10, _textSizeTop+2*padding)
    ctx.drawBreakingText(top, 10, _textSizeTop+2*padding, null, 1, 'fill');

    // draw bottom text

    ctx.fillStyle = '#000';
    style('basic-serif', _textSizeBottom, 'left', 'top');
    var height = ctx.drawBreakingText(bottom, 10, 0, null, 1, 'none').textHeight;
    ctx.drawBreakingText(bottom, 10,  _textSizeTop+_textSizeBottom, null, 1, 'fill');

    ctx.fillStyle='#fff';
    ctx.fillRect(0,0,0, 200);

  }




  document.getElementById('textSizeTop').value = textSizeTop;
  document.getElementById('textSizeBottom').value = textSizeBottom;
  document.getElementById('textSizeTopOut').innerHTML = textSizeTop;
  document.getElementById('textSizeBottomOut').innerHTML = textSizeBottom;


})
